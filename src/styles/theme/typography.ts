import { createTheme } from '@mui/material'
import { FontStyle, TypographyOptions } from '@mui/material/styles/createTypography'

const theme = createTheme()

export const openSansLight = 'open-sans-light, sans-serif;' // 300
export const interLight = 'inter-light, sans-serif' // 300
export const interRegular = 'inter-regular, sans-serif' // 400
export const interMedium = 'inter-medium, sans-serif' // 500
export const interSemibold = 'inter-semibold, sans-serif' // 600
export const interBold = 'inter-bold, sans-serif' // 700

const fontStyle: FontStyle = {
    fontFamily: interRegular,
    fontSize: 14,
    htmlFontSize: 16,
    fontWeightLight: 400,
    fontWeightRegular: 400,
    fontWeightMedium: 400,
    fontWeightBold: 400,
}

export const typography: TypographyOptions = {
    ...fontStyle,
    body1: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(16),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '150%',
        letterSpacing: '0.15px',
    },
    body2: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(14),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '143%',
        letterSpacing: 'unset',
    },
    subtitle1: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(16),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '175%',
        letterSpacing: '0.15px',
    },
    subtitle2: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interMedium,
        fontSize: theme.typography.pxToRem(14),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '157%',
        letterSpacing: '0.1px',
    },
    overline: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(12),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '266%',
        letterSpacing: '1px',
        textTransform: 'uppercase',
    },
    caption: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(12),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '166%',
        letterSpacing: '0.4px',
    },
    h1: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interLight,
        fontSize: theme.typography.pxToRem(96),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '116.7%',
        letterSpacing: '-1.5px',
    },
    h2: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interLight,
        fontSize: theme.typography.pxToRem(60),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '120%',
        letterSpacing: '-0.5px',
    },
    h3: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(48),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '116.7%',
    },
    h4: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(34),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '123.5%',
    },
    h5: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interRegular,
        fontSize: theme.typography.pxToRem(24),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '133.5%',
    },
    h6: {
        fontFeatureSettings: "'clig' off, 'liga' off",
        fontFamily: interMedium,
        fontSize: theme.typography.pxToRem(20),
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: '160%',
        letterSpacing: '0.15px',
    },
}
