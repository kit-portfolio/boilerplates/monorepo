export const breakpoints = {
    values: {
        mobile: 0, // xs
        tablet: 600, //sm
        laptop: 900, // md
        desktop: 1200, // lg
        fullWidth: 1536, // xl
    },
}
