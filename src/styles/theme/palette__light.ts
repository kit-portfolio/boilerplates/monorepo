import { red, orange, green,blue,amber } from '@mui/material/colors'
import { PaletteOptions, alpha } from '@mui/material'

export const palette__light: PaletteOptions = {
    mode: 'light',
    divider: 'rgb(27, 36, 50, 0.12)',
    primary: {
        main: blue[800],
    },
    secondary: {
        main: amber[800],
    },
    text: {
        primary: '#2E3B54',
        secondary: alpha('#001238', 0.6),
        disabled: alpha('#2E3B54', 0.38),
    },
    error: {
        main: red[700],
    },
    warning: {
        main: orange[900],
    },
    info: {
        main: '#809FF5',
    },
    success: {
        main: green[800],
    },
}
