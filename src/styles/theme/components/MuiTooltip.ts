import { Components, Theme } from '@mui/material'

export const MuiTooltip: Components<Omit<Theme, 'components'>>['MuiTooltip'] = {
    styleOverrides: {
        tooltip: () => ({
            background: 'aliceblue',
            borderRadius: 1,
        }),
        arrow: {
            color: 'aliceblue',
        },
    },
}
