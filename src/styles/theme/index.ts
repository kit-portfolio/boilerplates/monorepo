import { createTheme, PaletteMode } from '@mui/material'
import { palette__light } from './palette__light'
import { palette__dark } from './palette__dark'
import { breakpoints } from './breakpoints'
import * as components from './components'
import { shape } from './shape'
import { typography } from './typography'
import { shadows } from './shadows'

export const theme = (mode: PaletteMode) =>
    createTheme({
        breakpoints,
        components,
        palette: mode === 'light' ? palette__light : palette__dark,
        shape,
        shadows,
        typography,
        spacing: 4,
    })
