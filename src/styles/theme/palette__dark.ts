import {red, orange, green, blue, amber} from '@mui/material/colors'
import { PaletteOptions, alpha } from '@mui/material'

export const palette__dark: PaletteOptions = {
    mode: 'dark',
    divider: 'rgb(255, 255, 255, 0.12)',
    primary: {
        main: blue[400],
    },
    secondary: {
        main: amber[400],
    },
    text: {
        primary: '#FFF',
        secondary: alpha('#FFF', 0.7),
        disabled: alpha('#FFF', 0.38),
    },
    error: {
        main: red[500],
    },
    warning: {
        main: orange[400],
    },
    info: {
        main: '#DEE6FC',
    },
    success: {
        main: green[400],
    },
}
