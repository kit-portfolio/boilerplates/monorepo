/**
 * Enum naming convention enforcer
 *
 * Protocol:
 * 1. Retrieve all project files
 * 2. Extract enums
 * 3. Rename enums and their references in according to naming convention
 */

import { Project } from 'ts-morph'
import _ from 'lodash'
// Initialize a project with our tsconfig file
const project = new Project({
    tsConfigFilePath: '../../../tsconfig.json',
})
// Get all project files
const sourceFiles = project.getSourceFiles()
sourceFiles.forEach((sourceFile) => {
    console.log(':point_right:', sourceFile.getBaseName())
    // Get all enums in a file
    const enums = sourceFile.getEnums()
    enums.forEach((enm) => {
        // Get enum name, create a duplicate to ensafe further manipulations
        const name = enm.getName()
        let refactoredName = name
        refactoredName = _.snakeCase(refactoredName).toUpperCase()
        // Rename enum
        console.log(name, '->', refactoredName)
        enm.rename(refactoredName, {
            renameInComments: false,
            renameInStrings: false,
        })
        const enumMembers = enm.getMembers()
        // Enum members processing
        enumMembers.forEach((member) => {
            // Get enum member name, create a duplicate to ensafe further manipulations
            const name = member.getName()
            let refactoredName = name
            refactoredName = _.snakeCase(refactoredName).toUpperCase()
            // Rename enum member
            console.log(name, '->', refactoredName)
            member.rename(refactoredName, {
                renameInComments: false,
                renameInStrings: false,
            })
        })
    })
    console.log()
})
// Save all changed files
project.saveSync()
