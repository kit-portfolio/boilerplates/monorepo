/**
 * Interface naming convention enforcer
 *
 * Protocol:
 * 1. Retrieve all project files
 * 2. Extract interfaces
 * 3. Rename interfaces and their references in according to naming convention
 */

import { Project } from 'ts-morph'
// Initialize a project with our tsconfig file
const project = new Project({
    tsConfigFilePath: '../../../tsconfig.json',
})
// Get all project files
const sourceFiles = project.getSourceFiles()
sourceFiles.forEach((sourceFile) => {
    console.log(':point_right:', sourceFile.getBaseName())
    // Get all interfaces in a file
    const interfaces = sourceFile.getInterfaces()
    interfaces.forEach((i) => {
        // Get interface name, create a dublicate to ensafe further manipulations
        const name = i.getName()
        let refactoredName = name
        // Adding prefix
        if (refactoredName.slice(0, 2) !== 'I_') {
            refactoredName = `I_${name}`
        }
        // Validating props suffix
        if (
            refactoredName.slice(refactoredName.length - 5, refactoredName.length) === 'Props' &&
            refactoredName[refactoredName.length - 6] !== '_'
        ) {
            refactoredName = refactoredName.slice(0, refactoredName.length - 5) + '_Props'
        }
        // Rename interface
        console.log(name, '->', refactoredName)
        i.rename(refactoredName, {
            renameInComments: false,
            renameInStrings: false,
        })
    })
    console.log()
})
// Save all changed files
project.saveSync()
