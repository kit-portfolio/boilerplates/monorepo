import { useToast } from 'design/templates/toast-provider'
import { locale } from 'locales'

interface ToastOptions {
    error?: string
    pending?: string
    success?: string
}
export const useApiToastDecorator = () => {
    const { showToast } = useToast()

    const request = (rq: Promise<any>, toast: ToastOptions) => {
        const {
            error = locale.messages.errors.generic,
            pending = locale.messages.info.loading,
            success = locale.messages.info.loadingCompleted,
        } = toast

        showToast({ message: pending, type: 'info', duration: null })

        rq
            .then(() => showToast({ message: success, type: 'success' }))
            .catch(() => showToast({ message: error, type: 'error' }))

        return rq
    }

    return { request }
}
