import {useCallback} from "react";
import {useToast} from "design/templates/toast-provider/toast-provider.hooks";
import {locale} from "locales";

export const useCopyToClipboard = () => {
  const {showToast} = useToast()

  const copyTextToClipboard = useCallback((text: string) => {
    navigator.clipboard.writeText(text);
    showToast({type: 'info', message: locale.messages.info.copied})
  }, [])

  return { copyTextToClipboard };
};
