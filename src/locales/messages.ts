export default {
    errors: {
        generic: 'Something went wrong.',
        accessDenied: 'Your access to this page is forbidden.',
    },
    callToAction: {
        refresh: 'Please, refresh the page.',
        contactUs: {
            neutral: 'Contact us',
            forHelp: 'Please, contact us for help',
        },
    },
    info: {
        copied: 'Copied to clipboard!',
        loading: 'Loading',
        loadingCompleted: 'Loading completed',
    },
    entity: {
        activation: {
            inProgress: (entity: string) => `Activating ${entity}`,
            failed: (entity: string) => `Failed to activate ${entity}`,
            completed: (entity: string) => `${entity} activated`,
        },
        copying: {
            inProgress: (entity: string) => `Copying ${entity}`,
            failed: (entity: string) => `Failed to copy ${entity}`,
            completed: (entity: string) => `${entity} copied`,
        },
        create: {
            inProgress: (entity: string) => `Creating ${entity}`,
            failed: (entity: string) => `Failed to create ${entity}`,
            completed: (entity: string) => `${entity} created`,
        },
        change: {
            inProgress: (entity: string) => `Changing ${entity}`,
            failed: (entity: string) => `Failed to change ${entity}`,
            completed: (entity: string) => `${entity} changed`,
        },
        delete: {
            inProgress: (entity: string) => `Deleting ${entity}`,
            failed: (entity: string) => `Failed to delete ${entity}`,
            completed: (entity: string) => `${entity} deleted`,
        },
        discard: {
            inProgress: (entity: string) => `Discarding ${entity}`,
            failed: (entity: string) => `Failed to discard ${entity}`,
            completed: (entity: string) => `${entity} discarded`,
        },
        download: {
            inProgress: (entity: string) => `Downloading ${entity}`,
            failed: (entity: string) => `Failed to download ${entity}`,
            completed: (entity: string) => `${entity} downloaded`,
        },
        load: {
            inProgress: (entity: string) => `Loading ${entity}`,
            failed: (entity: string) => `Failed to load ${entity}`,
            completed: (entity: string) => `${entity} loaded`,
        },
        publish: {
            inProgress: (entity: string) => `Publishing ${entity}`,
            failed: (entity: string) => `Failed to publish ${entity}`,
            completed: (entity: string) => `${entity} published`,
        },
        republish: {
            inProgress: (entity: string) => `Republishing ${entity}`,
            failed: (entity: string) => `Failed to republish ${entity}`,
            completed: (entity: string) => `${entity} republished`,
        },
        rename: {
            inProgress: (entity: string) => `Renaming ${entity}`,
            failed: (entity: string) => `Failed to rename ${entity}`,
            completed: (entity: string) => `${entity} renamed`,
        },
        replace: {
            inProgress: (entity: string) => `Replacing ${entity}`,
            failed: (entity: string) => `Failed to replace ${entity}`,
            completed: (entity: string) => `${entity} replaced`,
        },
        reset: {
            inProgress: (entity: string) => `Resetting ${entity}`,
            failed: (entity: string) => `Failed to reset ${entity}`,
            completed: (entity: string) => `${entity} reset`,
        },
        save: {
            inProgress: (entity: string) => `Saving ${entity}`,
            failed: (entity: string) => `Failed to save ${entity}`,
            completed: (entity: string) => `${entity} saved`,
        },
        update: {
            inProgress: (entity: string) => `Updating ${entity}`,
            failed: (entity: string) => `Failed to update ${entity}`,
            completed: (entity: string) => `${entity} updated`,
        },
    },
}
