import { default as inputs } from './inputs'
import { default as messages } from './messages'
import { default as operations } from './operations'

export const locale = {
    inputs,
    messages,
    operations,
}
