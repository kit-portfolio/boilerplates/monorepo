export default {
    upload: 'Upload',
    auth: {
        signUp: 'Sign up',
        signIn: 'Sign in',
        signOut: 'Sign out',
        forgotPassword: 'Forgot password',
        resetPassword: 'Reset password',
    },
    entity: {
        copy: 'Copy',
        create: 'Create',
        delete: 'Delete',
        move: 'Move',
    },
    generic: {
        ok: 'OK',
        cancel: 'Cancel',
        save: 'Save',
    },
}
