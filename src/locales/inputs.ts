export default {
    creditCard: {
        number: {
            label: 'Card Number',
            placeholder: '**** **** **** ****',
        },
        cvv: {
            label: 'CVV',
            placeholder: '***',
        },
        expiry: {
            month: {
                label: 'Month',
                placeholder: 'mm',
            },
            year: {
                label: 'Year',
                placeholder: 'yyyy',
            },
        },
    },
    user: {
        firstName: {
            label: 'First Name',
            placeholder: 'Type your name',
        },
        lastName: {
            label: 'Last Name',
            placeholder: 'Type your last name',
        },
    },
    address: {
        zip: {
            label: 'ZIP code',
            placeholder: '#####',
        },
    },
}
