// MODULES
import { ValidationRule } from 'react-hook-form';
import { useTranslation } from 'next-i18next';

// RESOURCES
import {constraints} from "constants/constraints.constants";

export function useValidation() {
	const { t } = useTranslation(['form']);

	// =========================
	// COMMON VALIDATION METHODS
	// =========================

	interface IRequiredRules {
		required: ValidationRule<boolean>,
		setValueAs: ((v: any) => any) | undefined,
	}

	/**
     * @param message - custom validation message.
     */
	const isRequired: (message?: string) => IRequiredRules =
        (message = t('validations.isRequired', { ns : 'form' })) => ({
        	setValueAs : v => (typeof v === 'string' ? v.trim() : v),
        	required : {
        		value : true,
        		message : message!,
        	},
        });

	/**
     * @param limit - number of symbols limiting user input.
     * @param message - custom validation message.
     */
	const maxLength: (limit: number, message?: string) => { maxLength: ValidationRule<number> } =
        (limit, message = t('validations.maxLength', { ns : 'form', limit })) => ({
        	maxLength : {
        		value : limit,
        		message : message!,
        	},
        });

	/**
     * @param limit - number of symbols limiting user input.
     * @param message - custom validation message.
     */
	const minLength: (limit: number, message?: string) => { minLength: ValidationRule<number> } =
        (limit, message = t('validations.minLength', { ns : 'form', limit })) => ({
        	minLength : {
        		value : limit,
        		message : message!,
        	},
        });

	/**
     * @param limit - maximum allowed value.
     * @param message - custom validation message.
     */
	const maxValue: (limit: number, message?: string) => { max: ValidationRule<number> } =
        (limit, message = t('validations.maxValue', { ns : 'form', limit })) => ({
        	max : {
        		value : limit,
        		message : message!,
        	},
        });

	/**
     * @param limit - minimum allowed value.
     * @param message - custom validation message.
     */
	const minValue: (limit: number, message?: string) => { min: ValidationRule<number> } =
        (limit, message = t('validations.minValue', { ns : 'form', limit })) => ({
        	min : {
        		value : limit,
        		message : message!,
        	},
        });

	/**
     * @param pattern - RegExp used to validate input format.
     * @param message - custom validation message.
     */
	const validatePattern: (pattern: RegExp, message?: string) => { pattern: ValidationRule<RegExp> } =
        (pattern, message = t('validations.incorrectPattern', { ns : 'form' })) => ({
        	pattern : {
        		value : pattern,
        		message : message!,
        	},
        });

	// ==================
	// VALIDATION PRESETS
	// ==================

	interface IPasswordValidationRules {
		max: ValidationRule<number>;
		min: ValidationRule<number>;
	}
	const validateAge: () => IPasswordValidationRules =
        () => ({
        	...maxValue(constraints.user.age.max),
        	...minValue(constraints.user.age.min),
        });

	return {
		isRequired,
		maxLength,
		minLength,
		maxValue,
		minValue,
		validateAge,
		validatePattern,
	};
}
