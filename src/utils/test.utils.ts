const testIdPropName = 'data-test-id';

interface ITestIdProp {
	[testIdPropName]: string;
}

/**
 * Function intended to provide a unified test ID to various components.
 */
export function createTestId(testId?: string): ITestIdProp | null {
	// Insert environment verification. If non-prod - return null
	if (false) return null;

	return testId ? { [testIdPropName] : testId } : null;
}
