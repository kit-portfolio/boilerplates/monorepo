export const breakpoint = {
    tablet: 600,
    desktop: 1200,
    maxWidth: 600,
}
