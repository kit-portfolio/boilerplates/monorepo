/**
 * Routing model #2
 * Constant-oriented approach
 *
 * PROS
 * Allows you to reuse root values for specific areas
 * Supports isolated import
 * All routes are stored in constants
 *
 * CONS
 * Exporting all routes may be problematic
 * Unable to type application routes
 */
export const home = '/'
export const placeholder = '#'
export const settings = '/settings'
export const dashboard = '/dashboard'
export const userCatalog = '/users'
export const user = (id: string) => `${userCatalog}/${id}`
