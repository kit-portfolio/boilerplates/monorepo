/**
 * Routing model #1
 * A collection of nested objects with string values for static routes and functions for dynamic routes.
 *
 * Dynamic routes are implemented as functions with options argument.
 * This enables route params type checking without any additional code.
 * Also, options argument comes with default values, enabling us to create route-compliant links with no efforts
 *
 * PROS
 * Built-in no-code type check
 * Nested structure increases readability
 *
 * CONS
 * This model doesn't work well with optional route parameters.
 * In order to resolve this problem, you need to build helper functions
 */
export const route = {
    agency: {
        dashboard: 'agency-dashboard'
    },
    all: '*',
    home: '/',
    static: {
        contactsUs: '/contact-us',
        terms: '/terms',
        privacyPolicy: '/privacy',
    },
    goods: {
        byId: ({id} = {id: ':id'}) => `/item/${id}`,
    },
    placeholder: '#',
} as const

/**
 * KNOWN PROBLEMS
 *
 * Sometimes it may be hard to define root address for routes group.
 * I suggest to use utility routes like: index, root, catalog.
 *
 */
export const demo = {
    goods: {
        catalog: '/goods'
    },
    settings: {
        index: '/settings',
        account: '/settings/account',
        security: '/settings/security',
    },
} as const
