export const INTEGER_REGEX = /^-?\d*$/;
export const PHONE_REGEX = /^(\d{3})[ -]?(\d{3})[ -]?(\d{4}|\d{3})$/;
export const EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
export const DECIMAL_OR_INTEGER_REGEX = /^\d*\.?\d*$/;
