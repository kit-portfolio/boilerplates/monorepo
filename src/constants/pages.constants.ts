export enum RouteE {
    ABOUT = '/about',

    ARTIST_CREATE = '/profile/artist/create',

    BRAND_ASSETS = '/brand-assets',

    CARBON_ZERO = '/carbon-zero',
    CARBON_CALCULATOR = '/carbon-calculator',

    COLLECTIBLE_CREATE = '/nft/create',
    COLLECTIBLE_CREATE_BY_TYPE = '/nft/create/:createType',

    COLLECTIBLE_DETAILS = '/nft/:id',
    COLLECTIBLE_EDIT = '/nft/:id/edit',
    COLLECTIBLES = '/nft/:collectionType',

    COLLECTION_CREATE = '/collection/create',
    COLLECTION_EDIT = '/collection/edit/:id',
    COLLECTIONS_LIST = '/collections',
    COLLECTION_VIEW = '/collections/:id',

    EMAIL_CONFIRMATION = '/user/email-confirm/:code/:login',

    EXPLORE = '/explore',

    FAQ = '/faq',

    HOME = '/',

    HOT_BIDS = '/hot-bids',

    AUCTION_APPROVALS = '/auctions-approvals',

    MY_PROFILE = '/profile',
    MANAGE_ARTISTS = '/profile/artists',
    EDIT_ARTIST = '/profile/artist/edit/:id',
    ARTIST_PROFILE = '/artist/profile/:id',

    PLACEHOLDER = '#',

    PRIVACY = '/privacy',

    PROFILE = '/profile/:login',
    PROFILE_EDIT = '/profile/edit',

    SEARCH = '/search/:query',

    SET_NEW_PASSWORD =
        '/user/reset-password-confirmation/:code/:login_b64',

    TERMS = '/terms',

    USER_EDIT = '/account-settings',

    UNIVERSES = '/universes',
    UNIVERSE_1 = '/universes/1',
}

export interface SetNewPasswordRouteParamsI {
    code: string;
    login_b64: string;
}

/**
 * Universal route generator.
 * This method creates URI string based on provided params
 * @param route URI string to be modified.
 * Params should be provided with semicolon prefix.
 * Example: "https://example.com/:id"
 * @param params params object, where key is param name, and value is param value.
 * Example: {firstName: "John", lastName: "Doe"}
 */
export const generatePath = (route: string, params: {[key: string] : string | number}) =>
    Object
        .keys(params)
        .reduce((acum, paramKey) => (
            acum.replace(`:${paramKey}`, String(params[paramKey]))
        ), route);
