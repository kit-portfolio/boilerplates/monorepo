export const passwordLength = 8;
export const verifyLength = (input: string) => input.length >= passwordLength;
export const verifyDigits = (input: string) => /(?=.*\d)/.test(input);
export const verifyLowercase = (input: string) => /(?=.*[a-z])/.test(input);
export const verifyUppercase = (input: string) => /(?=.*[A-Z])/.test(input);
export const verifySpecialChars = (input: string) => /(?=.*[-+_!@#$%^&*.,?])/.test(input);
