import { useContext } from 'react'
import { Context, ThemeContext } from './theme-provider'

export const useThemeControls = (): ThemeContext => {
    const context = useContext(Context)

    if (!context) {
        throw new Error('useThemeControls hook must be used within a Theme context')
    }

    return context
}
