import { Alert, type AlertColor, Snackbar, Slide } from '@mui/material'
import { createContext, type ReactNode, useCallback, useMemo, useState } from 'react'

export interface ToastProviderProps {
    children: ReactNode
}

export interface ToastContext {
    showToast: (params: ToastParams) => void
    updateToast: (params: ToastParams) => void
    closeToast: () => void
}

export const Context = createContext({} as ToastContext)

export interface ToastParams {
    message: string
    title?: string
    variant?: 'filled' | 'standard' | 'outlined'
    type?: AlertColor
    action?: ReactNode
    icon?: ReactNode
    duration?: number | null
    persist?: boolean
}

const defaults: ToastParams = {
    message: '',
    title: '',
    variant: 'filled',
    type: 'info',
    action: null,
    icon: null,
    duration: 2000,
    persist: true,
}

const transitionDuration = 200

// TODO: [VID-6370] Enhance animation
export const ToastProvider = ({ children }: ToastProviderProps) => {
    const [toast, setToast] = useState<ToastParams>({
        ...defaults,
    })
    const [open, setOpen] = useState(false)

    const openToast = useCallback((params: ToastParams) => {
        setToast({ ...defaults, ...params })
        setOpen(true)
    }, [])

    const showToast = useCallback(
        (params: ToastParams) => {
            if (open) {
                if (toast.persist) return

                closeToast()
                setTimeout(() => openToast(params), transitionDuration)

                return
            }

            openToast(params)
        },
        [open],
    )

    const updateToast = useCallback(
        (params: ToastParams) => {
            setToast((prev) => ({ ...prev, ...params }))
        },
        [open],
    )

    const closeToast = useCallback(() => {
        setOpen(false)
    }, [])

    const contextValue = useMemo(
        () => ({
            showToast,
            updateToast,
            closeToast,
        }),
        [showToast, updateToast, closeToast],
    )

    const { action, message, icon, title, duration, variant, type } = toast

    return (
        <Context.Provider value={contextValue}>
            <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={open}
                title={title}
                autoHideDuration={duration}
                onClose={closeToast}
                TransitionComponent={Slide}
                transitionDuration={transitionDuration}
            >
                <Alert severity={type} variant={variant} action={action} icon={icon}>
                    {message}
                </Alert>
            </Snackbar>

            {children}
        </Context.Provider>
    )
}
