import { SxProps } from '@mui/material'
import { Theme } from '@mui/material/styles'

const background: SxProps<Theme> = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    direction: 'column',
}

const container: SxProps<Theme> = {
    maxWidth: 800,
    minWidth: 400,
    minHeight: 220,
    my: 0,
    mx: 8,
    backgroundColor: (theme) => theme.palette.background.paper,
    p: 6,
    borderRadius: 5,
}

const modalHeader: SxProps<Theme> = {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    gap: 4,
    userSelect: 'none',
}

const modalBody: SxProps<Theme> = {
    flexGrow: 1,
    position: 'relative',
}

const modalActions: SxProps<Theme> = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 4,
}

export default {
    background,
    container,
    modal: {
        header: modalHeader,
        body: modalBody,
        actions: modalActions,
    },
}







