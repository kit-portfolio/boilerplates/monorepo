import { ReactNode, useMemo } from 'react'
import { Box, BoxProps, Modal as MUIModal, Stack, IconButton, Typography } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import style from './modal.styles'

/** Props for the Modal component. */
interface ModalProps {
    /** Controls the visibility of the modal. */
    open: boolean
    /** The content to be rendered inside the modal. */
    children: ReactNode
    /** Callback fired when the component requests to be closed. */
    onClose(): void
}

/**
 * A customizable modal component.
 */
export const Modal = ({ children, open, onClose }: ModalProps) => {
    return (
        <MUIModal open={open} sx={style.background} onClose={onClose}>
            <Stack justifyContent="space-between" gap={6} sx={style.container}>
                {children}
            </Stack>
        </MUIModal>
    )
}

interface ModalHeaderProps extends BoxProps {
    /**
     * The title to be displayed in the modal header.
     * Be advised: it overrides Headers' children.
     * */
    title?: string
    /** Determines whether the close button should be displayed. */
    onClose?: () => void
}

/**
 * Represents the header section of the Modal.
 */
const Header = (props: ModalHeaderProps) => {
    const { title, children, onClose } = props

    const content = useMemo(() => {
        if (title) return <Typography variant="h5">{title}</Typography>
        if (children) return children
        return null
    }, [children, title])

    const closeButton = useMemo(() => {
        if (!onClose) return null

        return (
            <IconButton onClick={onClose}>
                <CloseIcon />
            </IconButton>
        )
    }, [onClose])

    return (
        <Box sx={style.modal.header} {...props}>
            {content}
            {closeButton}
        </Box>
    )
}

/**
 * Represents the body section of the Modal.
 */
const Body = (props: BoxProps) => {
    return (
        <Box sx={style.modal.body} {...props}>
            {props.children}
        </Box>
    )
}

/**
 * Represents the actions section of the Modal.
 */
const Actions = (props: BoxProps) => {
    if (!props.children) return null

    return (
        <Box sx={style.modal.actions} {...props}>
            {props.children}
        </Box>
    )
}

// Assigning components to Modal for easy access
Modal.Header = Header
Modal.Body = Body
Modal.Actions = Actions
