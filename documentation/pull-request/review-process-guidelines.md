# Review process guidelines

### Threads
* Threads can be closed only by topic starter.
* On close, PR threads should be finalized with short summary, like: `resolved`, `fixed`, `implemented`, `closed as non-actual`, `obsolete`, etc.
* If problem addressed is small, suggest change using native GitHub feature, to eliminate unnecessary discussions.
* When assignee finished processing change requests, he should re-request review.

### Drafting
* If PR created still has some work to do, or just not intended to be merged yet, author should make it draft
* Draft pull requests should be reviewed only upon request.

### Negotiating
If, during review, approach conflict appears, it should be resolved next way:
1. Discussion between parties involved (in thread, or via huddle, if situation requires it).
2. Discussion in team Slack channel.
3. Decision by next level manager.

4. Any of these steps may be omitted if conflict resolved, but order must be respected at all times.
