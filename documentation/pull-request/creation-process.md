### General ruleset
* PR names should comply with naming convention
* Author should assign himself to PR
* Labels should be used to outline PR type
* Allowed labels are: `feature`, `bug`, `refactor`, `proof of concept`, `refactor`
* If issue has priority `high` or `highest`, then `⇧ Urgent` label should be added.
* Urgent PR’s should be reviewed first

### Naming convention
1. PR title should include:
   * Issue id
   * Issue parent summary
   * Issue summary
2. All these parts should be united in following syntax: [issue_id] issue-parent-summary \ issue-summary

> Example: [VID-357] Storybook \ Setting up theme-compliant viewports
