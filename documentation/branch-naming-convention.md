### Regulations
* Branch name consists of three parts:
  * Prefix. Reference: `task` for tasks, `fix` for bugs, `feature` for story
  * Issue ID
  * Issue summary
* Branch name parts are united in following syntax: prefix/issue-id__issue-summary
> Example: feat/VID-555__killer-feature
* Special symbols in issue summary should be omitted
  * The only whitelisted special symbols are `-` and `_`

### Automation
This process can be fully automated using basic Jira features.
1. Trigger automation manually, or per state change.
2. Gather all ticket info you need via smart values.
3. Comment issue:
```
Here is your branch creation script:
{code:shell}git checkout -b {{branchName}}{code}
```
> **Be advised**:
> * Comment will become visible on page refresh, or after click on comment input
> * Don't forget to notify your readers about automated helpers
> * You can setup spam filter in comment generator, which will allow to post comment only once per issue
