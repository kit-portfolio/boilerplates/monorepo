### Localization
* All plain text inputs should be stored in dedicated locale files.
* There are two types of locales:
  * `Global`: stores generic textings, like operation names (create, save, delete), error\validation messages, or literally every text, that can possibly appear in any point of the system.
  * `Component`: stores textings, actual only for single component, like title or specific message.
* Global locales could only be imported in component locales
* Component locale should use default export to prevent locale namespace bloating
* Locale files should be organized into semantic structure.
* Base language locale object may serve as a type source for validating other language locales
