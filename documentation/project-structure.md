### Project structure
`src`  
* `api` - Backend API communication and integration  
* `design` - Root folder for components  
  * `atoms` - Smallest and simplest pieces of UI  
  * `molecules` - Combination of atoms (simple components)  
  * `organisms` - More complex components composed of molecules and atoms  
  * `templates` - Larger components that define page structure  
  * `pages` - Full-page components representing different views or routes  
* `hooks` - Custom React hooks  
* `locales` - Global localization maps files  
* `pages` - OBSOLETE - Top-level page components for routing  
* `styles` - Global styles and theme configuration  
* `types` - TypeScript type definitions and interfaces  
* `utils` - Utility functions and helper modules  

### Assets structuring
1. React components should be stored in `design` folder.
2. Generic components should be placed in folder in regard to their complexity.
> **Example**: Button goes to atoms folder, since it is small, simple piece of UI.
3. Components intended for limited usage in certain functional area, should be kept as close to this area component, as possible.
> **Example**: `SearchResults` component should be kept in `design/pages/search-page` folder, as it is ment to be used only in conjunction with SearchPage.
